#!/usr/bin/perl
#
# Copyright (c) 2018, 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


use v5.10;
use strict;
use warnings;

package Test::FeatureCheck;

use v5.10;
use strict;
use warnings;

use version; our $VERSION = version->declare("v0.2.2");

use parent qw(Exporter);

our @EXPORT_OK = qw(
	env_init
	get_error_output get_ok_output
	test_fcheck_init
);

sub env_init()
{
	my $prog = $ENV{TEST_PROG} // './feature-check';

	return (
		fcheck => $ENV{TEST_FCHECK} // './t/bin/fcheck',
		is_python => $prog =~ m{ /python/ }x ? 1 : 0,
		prog => $prog,
	);
}

sub get_ok_output($ $) {
	my ($cmd, $desc) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	$c->exit_is_num(0, "$desc succeeded");
	$c->stderr_is_eq('', "$desc did not output any errors");
	split /\n/, $c->stdout_value
}

sub get_error_output($ $) {
	my ($cmd, $desc) = @_;

	my $c = Test::Command->new(cmd => $cmd);
	$c->exit_isnt_num(0, "$desc failed");
	$c->stdout_is_eq('', "$desc did not output anything");
	split /\n/, $c->stderr_value
}

sub test_fcheck_init($) {
	my ($c) = @_;
	my $fcheck = $c->{fcheck};

	if (! -f $fcheck || ! -x $fcheck) {
		Test::More::BAIL_OUT("The '$fcheck' program does not exist ".
		    "or is not executable");
	}
	$ENV{FCHECK_TEST_OPT} = '--features';
	$ENV{FCHECK_TEST_PREFIX} = 'Features: ';
	my $fcheck_base;
	Test::More::subtest 'Make sure the fcheck helper works' => sub {
		my @lines = get_ok_output([$fcheck, '--features'],
		    'fcheck features');
		Test::More::is scalar @lines, 1,
		    'fcheck --features output a single line';
		Test::More::BAIL_OUT("No 'Features: ' on the '$fcheck' ".
		    "features line") unless
		    $lines[0] =~ /^ Features: \s (?<features> .* ) $/x;
		my @words = split /\s+/, $+{features};
		my %names = map { split /[:\/=]/, $_, 2 } @words;
		Test::More::BAIL_OUT("No 'base' in the '$fcheck' ".
		    "features list") unless defined $names{base};
		$fcheck_base = $names{base};
		Test::More::BAIL_OUT("Found 'x' in the '$fcheck' ".
		    "features list") if defined $names{x};
	};
	return $fcheck_base;
}

1;
