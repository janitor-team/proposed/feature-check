#!/usr/bin/make -f
#
# Copyright (c) 2018  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

PROG=		feature-check

MAN1=		feature-check.1
MAN1GZ=		${MAN1}.gz

INSTALL?=	install
MKDIR?=		mkdir -p
RM?=		rm -f
FALSE?=		false

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man
DOCSDIR?=	${PREFIX}/share/doc/feature-check
EXAMPLESDIR?=	${DOCSDIR}/examples

BINOWN?=	root
BINGRP?=	wheel
BINMODE?=	555

SHAREOWN?=	root
SHAREGRP?=	wheel
SHAREMODE?=	644

COPY?=		-c
STRIP?=		-s

INSTALL_PROGRAM?=	${INSTALL} ${COPY} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=		${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

all:		${PROG} ${MAN1GZ}

clean:
		${RM} ${PROG} ${MAN1GZ}

install:	all
		${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_SCRIPT} ${PROG} ${DESTDIR}${BINDIR}
		${MKDIR} ${DESTDIR}${MANDIR}1
		${INSTALL_DATA} ${MAN1GZ} ${DESTDIR}${MANDIR}1
		${MKDIR} ${DESTDIR}${DOCSDIR}
		${INSTALL_DATA} README.md ${DESTDIR}${DOCSDIR}

test-single:	${TEST_PROG}
		echo "Testing ${TEST_PROG}"
		prove t
		echo "Testing ${TEST_PROG} complete"

test-real:	${PROG}
		${MAKE} test-single TEST_PROG="./${PROG}"

test:		test-real

test-all:
		(set -e; for f in \
			perl5/feature-check.pl \
			python/2/feature-check.sh python/3/feature-check.sh \
			; do \
			if [ "$${f#python}" != "$$f" ]; then \
				flake8 "$$(dirname "./$$f")/../feature_check"; \
			fi; \
			${MAKE} test-single TEST_PROG="./$$f"; \
		done)
		@echo 'Testing all the feature-check implementations complete'

${PROG}:	perl5/feature-check.pl
		${INSTALL} -m 755 perl5/feature-check.pl ${PROG}

${MAN1GZ}:	${MAN1}
		gzip -c9 ${MAN1} > ${MAN1GZ}.tmp || (${RM} ${MAN1GZ}.tmp; ${FALSE})
		mv ${MAN1GZ}.tmp ${MAN1GZ} || (${RM} ${MAN1GZ} ${MAN1GZ}.tmp; ${FALSE})

.PHONY:		all clean test test-real test-single test-all
