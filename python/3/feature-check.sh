#!/bin/sh

env PYTHONPATH="$(dirname -- "$(dirname -- "$0")")${PYTHONPATH+${PYTHONPATH}}" \
    python3 -m feature_check "$@"
